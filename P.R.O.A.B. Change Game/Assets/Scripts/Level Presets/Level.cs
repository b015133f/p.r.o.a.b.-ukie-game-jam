﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level {
    /// <summary>
    /// Name of the Level for menu selection
    /// </summary>
    public string LevelName;
    /// <summary>
    /// Time limit to complete the Level, -1 for Endless
    /// </summary>
    public int TimeLimit;
    /// <summary>
    /// Number of Enemies that will spawn in total
    /// </summary>
    public int EnemyCount;
    /// <summary>
    /// Max number of Enemies that will be present on the map at one time
    /// </summary>
    public int EnemyMaxSpawned;
    /// <summary>
    /// Spawn Delay on an Enemy, At load all Enemies will be spawned at the same time.
    /// If Max enemy count is less than enemy count when space is available this duration in seconds is waited before spawning.
    /// </summary>
    public int EnemySpawnDelay;
    /// <summary>
    /// Score target for the player to aim for
    /// </summary>
    public int ScoreTarget;
}
