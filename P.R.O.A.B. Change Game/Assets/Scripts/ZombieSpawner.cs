﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject zombiePrefab;
    GameObject currentZombie;
    int counter;

    public int spawnTimer, maxNoOfZombiesOnScreen;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        counter++;
        if (counter > spawnTimer)
        {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length < maxNoOfZombiesOnScreen)
            {
                counter = 0;
                Instantiate(zombiePrefab);
            }
        }
	}
}
