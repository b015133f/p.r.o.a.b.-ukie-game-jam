﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinDisplay : MonoBehaviour
{
    public Sprite onePence;
    public Sprite twoPence;
    public Sprite fivePence;
    public Sprite tenPence;
    public Sprite twentyPence;
    public Sprite fiftyPence;
    public Sprite onePound;

    private int imageNumber = 0;
    void Start()
    {

    }

    public void Update()
    {
        if(LevelManager.Instance.GetLevelState() == LevelManager.LevelState.Playing)
        {
            if (Input.GetKeyDown("1"))
                imageNumber = 1;

            if (Input.GetKeyDown("2"))
                imageNumber = 2;

            if (Input.GetKeyDown("3"))
                imageNumber = 3;

            if (Input.GetKeyDown("4"))
                imageNumber = 4;

            if (Input.GetKeyDown("5"))
                imageNumber = 5;

            if (Input.GetKeyDown("6"))
                imageNumber = 6;

            if (Input.GetKeyDown("7"))
                imageNumber = 7;

            switch (imageNumber)
            {
                case 1:
                    this.gameObject.GetComponent<Image>().sprite = onePence;
                    break;

                case 2:
                    this.gameObject.GetComponent<Image>().sprite = twoPence;
                    break;

                case 3:
                    this.gameObject.GetComponent<Image>().sprite = fivePence;
                    break;

                case 4:
                    this.gameObject.GetComponent<Image>().sprite = tenPence;
                    break;

                case 5:
                    this.gameObject.GetComponent<Image>().sprite = twentyPence;
                    break;

                case 6:
                    this.gameObject.GetComponent<Image>().sprite = fiftyPence;
                    break;
                case 7:
                    this.gameObject.GetComponent<Image>().sprite = onePound;
                    break;
            }
        }

     
    }

}


