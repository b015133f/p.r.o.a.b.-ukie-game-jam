﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zombie : MonoBehaviour
{
    Vector2 thisPos, playerPos, healthBarSize;

    Text healthText;

    public int mapSize;

    public int tempScore;

    float maxHealth, currentHealth, movementSpeed, coinsRequiredToKill, numberOfTimesHit;

    const float minSpeed = 5.0f;
    const float maxSpeed = 20.0f;

    GameObject healthBar, animation;

    // Use this for initialization
    void Start ()
    {
        
        healthText = transform.GetComponentInChildren<Text>();
        foreach (Transform child in transform)
        {
            switch (child.tag)
            {
                case ("HealthBar"):
                    healthBar = child.gameObject;
                    break;
                case ("Animation"):
                    animation = child.gameObject;
                    break;
            }
        }

       

        GenerateHealth();
        GenerateSpawnLocation();

        currentHealth = maxHealth;
        healthBarSize.y = 20.0f;

        playerPos = GameObject.FindGameObjectWithTag("Player").transform.localPosition;

        movementSpeed = Random.Range(minSpeed / 1000, maxSpeed / 1000);

        UpdateRotation();

    }

    // Update is called once per frame
    void Update ()
    {
        if(LevelManager.Instance.GetLevelState() == LevelManager.LevelState.Playing) {
            transform.position = Vector2.MoveTowards(transform.position, playerPos, movementSpeed);

            System.Decimal amount = System.Convert.ToDecimal(currentHealth / 100);
            healthText.text = "£" + amount.ToString("0.##"); 
            healthBarSize.x = (currentHealth / maxHealth) * 100.0f;
            healthBar.GetComponent<RectTransform>().sizeDelta = healthBarSize;

            CheckIfDead();
        }
    }

    void CheckIfDead()
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);

            LevelManager.Instance.AddToScore((int)Mathf.Ceil(100 * (coinsRequiredToKill / numberOfTimesHit)));
        }
    }
	
    
    void GenerateHealth()
    {
        maxHealth = (int)Mathf.Ceil(Random.Range(0.0f, 188.0f));
        CalculateMinimumCoinsRequired();
    }

    void CalculateMinimumCoinsRequired()
    {
        CoinRounding(CoinRounding(CoinRounding(CoinRounding(CoinRounding(CoinRounding(CoinRounding(maxHealth, 100), 50), 20), 10), 5), 2), 1);
    }

    float CoinRounding(float h, float c)
    {
        float x = Mathf.Floor(h / c);
        coinsRequiredToKill += x;
        return (h - (c * x));
    }

    void GenerateSpawnLocation()
    {
        Vector2 pos = new Vector2();
        switch ((int)Mathf.Ceil(Random.Range(0.0f, 3.0f)))
        {
            case 1:
                pos.x = -mapSize;
                pos.y = Random.Range(0, mapSize);
                break;
            case 2:
                pos.x = mapSize;
                pos.y = Random.Range(0, mapSize);
                break;
            case 3:
                pos.x = Random.Range(-mapSize, mapSize);
                pos.y = mapSize;
                break;
        }
        transform.localPosition = pos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BulletScript bullet = collision.GetComponent<BulletScript>();
        int coinValue = bullet.coinValue;

        if (coinValue <= currentHealth)
            currentHealth -= coinValue;
        numberOfTimesHit += 1;
    }

    void UpdateRotation()
    {
        Vector3 difference = GameObject.FindGameObjectWithTag("Player").transform.localPosition - transform.localPosition;
        difference.Normalize();
        float rotation_z = (Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg - 90.0f);

        Quaternion myrotation = Quaternion.AngleAxis(rotation_z, Vector3.forward);
        animation.transform.rotation = myrotation;
    }
    
    public void AddToHealth(float n)
    {
        currentHealth += n;
    
        if(currentHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
