﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy
{

    public static GameObject NewZombie()
    {
        GameObject z = new GameObject("Zombo", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image), typeof(Zombie), typeof(CircleCollider2D), typeof(Rigidbody2D), typeof(AudioSource));
        z.GetComponent<Image>().color = new Color32(0, 0, 0, 0);
        z.GetComponent<Zombie>().mapSize = 2000;
        z.GetComponent<CircleCollider2D>().radius = 50;
        z.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        z.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
        z.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
        z.tag = "Enemy";

        GameObject t = new GameObject("Health", typeof(RectTransform), typeof(CanvasRenderer), typeof(Text));
        t.transform.SetParent(z.transform);
        t.transform.localScale = new Vector3(1.0f, 1.0f);
        t.transform.localPosition = new Vector3(0.0f, 60.0f);
        t.GetComponent<Text>().alignment = TextAnchor.UpperCenter;
        t.GetComponent<Text>().font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        t.GetComponent<Text>().fontSize = 30;
        t.GetComponent<Text>().color = new Color32(0, 0, 0, 255);

        GameObject a = new GameObject("Animation", typeof(SpriteRenderer), typeof(Animator));
        a.transform.SetParent(z.transform);
        a.transform.localScale = new Vector3(100.0f, 100.0f);
        a.transform.localPosition = new Vector3(0.0f, 0.0f, -5.8f);
        a.GetComponent<Animator>().runtimeAnimatorController = Resources.Load("Animations/z" + Mathf.Floor(Random.Range(1.0f, 4.0f)).ToString() + "_0") as RuntimeAnimatorController;
        a.tag = "Animation";

        GameObject h = new GameObject("Health Bar", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image));
        h.transform.SetParent(z.transform);
        h.GetComponent<RectTransform>().pivot = new Vector2(0.0f, 0.5f);
        h.GetComponent<RectTransform>().localPosition = new Vector3(-50.0f, 70.0f, 0.0f);
        h.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f);
        h.GetComponent<RectTransform>().sizeDelta = new Vector2(100.0f, 20.0f);
        h.GetComponent<Image>().color = new Color32(13, 190, 3, 255);
        h.tag = "HealthBar";

        return z;
    }
}
