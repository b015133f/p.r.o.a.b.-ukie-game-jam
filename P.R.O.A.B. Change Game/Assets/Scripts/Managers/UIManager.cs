﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Sprite FullHeart;
    public Sprite EmptyHeart;
    private Image[] PlayerHearts;
    
    public void Start()
    {
        PlayerHearts = new Image[3] { GameObject.Find("Heart1").GetComponent<Image>(), GameObject.Find("Heart2").GetComponent<Image>(), GameObject.Find("Heart3").GetComponent<Image>() };
        ResetHearts();
    }

    public void ResetHearts()
    {
        foreach (var Heart in PlayerHearts)
        {
            Heart.sprite = FullHeart;
        }
    }

    void Update()
    {
    }

    public void SetLivesDisplay(int Lives)
    {
        for (int i = 0; i < PlayerHearts.Length; i++)
        {
            if(Lives > i)
                PlayerHearts[i].sprite = FullHeart;
            else
                PlayerHearts[i].sprite = EmptyHeart;
        }
    }
}
