﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : Singleton<EnemyManager>
{
    private GameObject[] enemies;
    private Level level;

    int counter, enemiesSpawned;

    public EnemyManager()
    {
        if (LevelManager.Instance.LevelLoaded)
        {
            level = LevelManager.Instance.CurrentLevel;
            enemies = new GameObject[level.EnemyCount];
        }
    }

    void Update()
    {
        counter++;
        if (counter > level.EnemySpawnDelay)
        {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length < level.EnemyMaxSpawned && enemiesSpawned < level.EnemyCount)
            {
                enemies[enemiesSpawned] = Enemy.NewZombie();
                counter = 0;
                enemiesSpawned++;
            }
        }
    }

    
}
