﻿using System;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{   
    public EnemyManager EnemyManager;

    public GameObject PauseGameOverScreen;

    private int Highscore;
    private int HighestTime;
    
    private void Start()
    {
        LevelManager.Instance.LoadLevel("Level1");
        EnemyManager = EnemyManager.Instance;

        Highscore = PlayerPrefs.GetInt("HighScore");
        HighestTime = PlayerPrefs.GetInt("HighTime");
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (LevelManager.Instance.GetLevelState() == LevelManager.LevelState.Playing)
                LevelManager.Instance.SetLevelState(LevelManager.LevelState.Paused);

            else if (LevelManager.Instance.GetLevelState() == LevelManager.LevelState.Paused) {

                PauseGameOverScreen.SetActive(false);
                LevelManager.Instance.SetLevelState(LevelManager.LevelState.Playing);
            }
        }

        var levelState = LevelManager.Instance.GetLevelState();
        if (levelState == LevelManager.LevelState.Playing)
        {
            var timeLeft = LevelManager.Instance.GetLevelElapsedTime();
            GameObject.Find("ElapsedTimer").GetComponent<Text>().text = timeLeft.ToString("mm:ss");

            GameObject.Find("Score").GetComponent<Text>().text = LevelManager.Instance.GetScore().ToString() + " Top: " + Highscore;


        }
        else
        {
            LevelManager.Instance.CheckHighScore();

            PauseGameOverScreen.SetActive(true);
            var MenuElements = PauseGameOverScreen.GetComponentsInChildren<Text>();

            if (levelState == LevelManager.LevelState.Paused) 
            {
                MenuElements[0].text = "PAUSED";
            }
            else if(levelState == LevelManager.LevelState.GameOver)
            {
                MenuElements[0].text = "GAMEOVER";
            }
            MenuElements[3].text = LevelManager.Instance.GetScore().ToString();
            MenuElements[5].text = Highscore.ToString();
            MenuElements[7].text = LevelManager.Instance.GetLevelElapsedTime().ToString("mm:ss");
            //LevelManager.Instance.ResetLevelTimer();
        }
    }

    public void OnDestroy()
    {
        //LevelManager.Instance.StopTimer();
    }

    public void RestartButtonClick()
    {
        Debug.Log("RestartButton clicked");
    }

}
