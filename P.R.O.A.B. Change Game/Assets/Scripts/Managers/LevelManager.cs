﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class LevelManager : Singleton<LevelManager> {

    public Level CurrentLevel;
    public bool LevelLoaded;
    public bool LevelRunning;

    private bool LevelTimerExpired;
    private bool LevelFailed;
    private Timer LevelTimer;
    private int TimeElapsed;

    private int score;

    [SerializeField]
    private LevelState levelState;

    public void LoadLevel(string levelName)
    {
        LevelLoaded = false;
        var level = FileSystem.ReadDataFromFile<Level>(FileSystem.FileType.Levels, levelName);

        if (level != null && !string.IsNullOrEmpty(level.LevelName))
        {
            CurrentLevel = level;
            LevelLoaded = true;
            StartLevelTimer();
        }
    }

    private void StartLevelTimer()
    {
        LevelTimer = new Timer
        {
            Interval = 1000,
            AutoReset = true,
            Enabled = true
        };
        LevelTimer.Elapsed += LevelTimer_Elapsed;
        TimeElapsed = 0;
    }

    private void LevelTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
        if(levelState == LevelState.Playing)
        {
            TimeElapsed++;

            LevelManager.Instance.AddToScore(5);
        }
    }
    
    /// <summary>
    /// Best used for UI. Shows time elapsed on the level.
    /// </summary>
    /// <returns></returns>
    public DateTime GetLevelElapsedTime()
    {
        return new DateTime().AddSeconds(TimeElapsed);
    }

    public void AddToScore(int s)
    {
        score += s;
    }

    public int GetScore()
    {
        return score;
    }

    public void CheckHighScore()
    {
        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", score);
        }

        if (TimeElapsed > PlayerPrefs.GetInt("HighTime"))
        {
            PlayerPrefs.SetInt("HighTime", TimeElapsed);
        }
    }

    public void SetLevelState(LevelState state)
    {
        levelState = state;

        if (levelState == LevelState.Playing)
            LevelTimer.Enabled = true;
        else 
            LevelTimer.Enabled = false;

    }
    public LevelState GetLevelState() { return levelState; }

    public enum LevelState
    {
        Playing,
        Paused,
        GameOver
    }
}
