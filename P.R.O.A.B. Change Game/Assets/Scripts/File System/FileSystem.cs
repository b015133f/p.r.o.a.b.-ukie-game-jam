﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class FileSystem {

    public enum FileType
    {
        Levels,
        Setting,
        Player,
        Highscore,
        Instructions
    }

    public static T ReadDataFromFile<T>(FileType _fileType, string _fileName)
    {
        string filePath = Application.dataPath + "/" + _fileType.ToString() + "/" + _fileName + ".txt";
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            return JsonUtility.FromJson<T>(dataAsJson);
        }
        return default(T);
    }
}
