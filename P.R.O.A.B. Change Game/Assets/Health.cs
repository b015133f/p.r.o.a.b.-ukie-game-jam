﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public AudioClip pickUpSound;

    AudioSource source;

    // Use this for initialization
    void Start ()
    {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        source.PlayOneShot(pickUpSound, 1.0f);
        int PlayerLives = GameObject.FindObjectOfType<Player>().PlayerLives;
        PlayerLives++;

        if(PlayerLives <= 3)
        {
            GameObject.FindObjectOfType<Player>().PlayerLives = PlayerLives;
            GameObject.FindObjectOfType<UIManager>().SetLivesDisplay(PlayerLives);
        }
        
        StartCoroutine(SelfDestuct());
    }

    IEnumerator SelfDestuct()
    {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
}
