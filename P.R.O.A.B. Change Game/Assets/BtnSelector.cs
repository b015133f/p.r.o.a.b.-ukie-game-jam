﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BtnSelector : MonoBehaviour {

    //public string newScene;
    //public string currentScene;
    public string nLNewScene;

    Scene newScene;

    public Camera optionCamera;
    public Transform cameraTransform;
    float cameraX;
    int cameraY;
   
    public void StartGame()
    {
        SceneManager.LoadScene(nLNewScene, LoadSceneMode.Single);		
	}

    public void TranslateCamera()
    {
        cameraTransform = optionCamera.transform;

        cameraTransform.transform.Translate(new Vector3(25, 0, -5));
    }

    public void TranslateCameraBack()
    {
        cameraTransform = optionCamera.transform;

        cameraTransform.transform.Translate(new Vector3(-25, 0, -5));
    }
    
    public void TranslateInstruction()
    {
        cameraTransform = optionCamera.transform;

        cameraTransform.transform.Translate(new Vector3(-25, 0, -5));

    }

    public void TranslateInstructionBack()
    {
        cameraTransform = optionCamera.transform;

        cameraTransform.transform.Translate(new Vector3(25, 0, -5));
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
