﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //public string newScene;
    public string currentScene;
    public string nLNewScene;

    Scene newScene;

    public void OnMouseClick()
    {
         SceneManager.LoadScene(nLNewScene);

        //SceneManager.GetSceneByName(level);

        newScene = SceneManager.GetSceneByName("SampleScene");

        SceneManager.SetActiveScene(newScene);

        SceneManager.UnloadSceneAsync(currentScene);
    }

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
