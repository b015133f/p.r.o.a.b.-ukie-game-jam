﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float offset = 0;
    public float fireRate = 1.0f;
    public Transform firePoint;
    public GameObject bullet;
    Vector3 mousePosition;

    //Audio
    public AudioClip shootSound;
    public AudioClip reloadSound;
    public AudioClip hurtSound;
    public AudioClip moanSound;
    public AudioClip deathSound;

    AudioSource source;

    private float timeCounter = 0.0f;
    private float randomTime = 5.0f;

    float timeUntilFire = 0;
    float timeForMoan = 0;
    Animator anim;
    public float speed = 1.5f;
    public int PlayerLives = 3;

	// Use this for initialization
	void Start ()
    {
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(LevelManager.Instance.GetLevelState() == LevelManager.LevelState.Playing)
        {
            if (timeCounter > randomTime)
            {
                randomTime = Random.Range(0.0f, 5.0f);
                timeCounter = 0.0f;
                source.PlayOneShot(moanSound, 1.0f);
            }

            timeCounter += Time.deltaTime;

            mousePosition = Input.mousePosition;

            Vector3 newMousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Vector2 direction = new Vector2(newMousePosition.x - firePoint.transform.position.x, newMousePosition.y - firePoint.transform.position.y);
            transform.up = direction;

            if (Input.GetMouseButtonDown(0) && Time.time > timeUntilFire)
            {
                source.PlayOneShot(shootSound, 1.0f);
                timeUntilFire = Time.time + 1 / fireRate;
                anim.SetTrigger("isShoot");
                Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);
            }
            ChangeCoin();
        }
  
    }

    void ChangeCoin()
    {
        if (Input.GetKeyDown("7"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/1 pound");
        }
        else if (Input.GetKeyDown("1"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/1p");
        }
        else if (Input.GetKeyDown("2"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/2p");
        }
        else if (Input.GetKeyDown("3"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/5p");
        }
        else if (Input.GetKeyDown("4"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/10p");
        }
        else if (Input.GetKeyDown("5"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/20p");
        }
        else if (Input.GetKeyDown("6"))
        {
            source.PlayOneShot(reloadSound, 1.0f);
            anim.SetTrigger("isReload");
            bullet = Resources.Load<GameObject>("prefab/50p");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            if (PlayerLives > 0)
            {
                PlayerLives--;
                source.PlayOneShot(hurtSound, 1.0f);
                GameObject.FindObjectOfType<UIManager>().SetLivesDisplay(PlayerLives);
            }
            else
            {
                //Plays on loop becuase of being hit mulitple times
                source.PlayOneShot(deathSound, 1.0f);
                LevelManager.Instance.SetLevelState(LevelManager.LevelState.GameOver);
            }
            Destroy(collision.gameObject);
        }
    }
}
