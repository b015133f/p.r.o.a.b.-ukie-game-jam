﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float destroyTime = 3.0f;
    public float bulletSpeed = 5.0f;
    public int coinValue = 0;
    
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(SelfDestuct());
    }

    IEnumerator SelfDestuct()
    {
        yield return new WaitForSeconds(destroyTime);
        Destroy(this.gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Translate(Vector2.up * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
    }
}
