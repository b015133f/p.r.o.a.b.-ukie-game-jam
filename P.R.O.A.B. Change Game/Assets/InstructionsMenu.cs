﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsMenu : MonoBehaviour {

    public Text InstructionsField;
	// Use this for initialization
	void Start () {
        var ins = FileSystem.ReadDataFromFile<Guide>(FileSystem.FileType.Instructions, "Instructions");
        InstructionsField.text = ins.Text;

    }
}
